# Pivot Lab
This is a security focused lab to help people learn both red teaming and blue teaming.

# IMPORTANT
This will open a port with a vulnerable web service on your host machine. Understand the risk this poses to your box!

# Getting Started
Run:

`sudo sysctl -w vm.max_map_count=262144`

Then:

`docker-compose up --build`

A vulnerable service is opened up on 127.0.0.1:80, while a Kibana instance is open on 127.0.0.1:5601. Happy hunting yourself!
